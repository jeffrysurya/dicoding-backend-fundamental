require('dotenv').config();

const Hapi = require('@hapi/hapi');
const Jwt = require('@hapi/jwt');
const Inert = require('@hapi/inert');
const path = require('path');

const users = require('./api/users');
const UsersServices = require('./services/Postgres/Users/UserServices');
const UsersValidator = require('./validator/UserValidator');

const authentication = require('./api/auth');
const AuthenticationServices = require('./services/Postgres/Authentication/AuthenticationService');
const AuthenticationValidator = require('./validator/AuthenticationValidator');
const TokenManager = require('./Utility/Token Manager');

const albums = require('./api/albums');
const AlbumServices = require('./services/Postgres/Albums/AlbumServices');
const AlbumValidator = require('./validator/AlbumValidator');

const songs = require('./api/songs');
const SongsService = require('./services/Postgres/Songs/SongServices');
const SongsValidator = require('./validator/SongValidator');

const playlists = require('./api/playlists');
const PlaylistServices = require('./services/Postgres/Playlist/PlaylistService');
const PlaylistValidator = require('./validator/PlaylistValidator');

const collaborations = require('./api/collaborations');
const CollaborationsServices = require('./services/Postgres/Collaboration/CollaborationService');
const CollaborationsValidator = require('./validator/CollaborationValidator');

const _exports = require('./api/export');
const ProducerService = require('./services/Rabbitmq/ProducerService');
const ExportValidator = require('./validator/ExportValidator');

const uploads = require('./api/uploads');
const StorageService = require('./services/Storage/StorageService');
const ImageValidator = require('./validator/ImageValidator');

const likes = require('./api/likes');
const LikesService = require('./services/Postgres/likes/likesService');

const CacheService = require('./services/Redis/CacheService');

const init = async () => {
  const albumsServices = new AlbumServices();
  const songsService = new SongsService();
  const usersService = new UsersServices();
  const authenticationService = new AuthenticationServices();
  const collaborationService = new CollaborationsServices();
  const playlistService = new PlaylistServices(collaborationService);
  const storageService = new StorageService(path.resolve(__dirname, 'api/uploads/file/images'));
  const cacheService = new CacheService();
  const likeService = new LikesService(cacheService);

  const server = Hapi.server({
    port: process.env.PORT,
    host: process.env.HOST,
    routes: {
      cors: {
        origin: ['*'],
      },
    },
  });

  await server.register([
    {
      plugin: Jwt,
    },
    {
      plugin: Inert,
    },
  ]);

  server.auth.strategy('openmusic_jwt', 'jwt', {
    keys: process.env.ACCESS_TOKEN_KEY,
    verify: {
      aud: false,
      iss: false,
      sub: false,
      maxAgeSec: process.env.ACCESS_TOKEN_MAX_AGE,
    },
    validate: (artifacts) => ({
      isValid: true,
      credentials: {
        id: artifacts.decoded.payload.id,
      },
    }),
  });

  await server.register([
    {
      plugin: albums,
      options: {
        service: albumsServices,
        validator: AlbumValidator,
      },
    },
    {
      plugin: songs,
      options: {
        service: songsService,
        validator: SongsValidator,
      },
    },
    {
      plugin: users,
      options: {
        service: usersService,
        validator: UsersValidator,
      },
    },
    {
      plugin: authentication,
      options: {
        authenticationService,
        usersService,
        validator: AuthenticationValidator,
        tokenManager: TokenManager,
      },
    },
    {
      plugin: playlists,
      options: {
        service: playlistService,
        validator: PlaylistValidator,
      },
    },
    {
      plugin: collaborations,
      options: {
        collaborationService,
        playlistService,
        validator: CollaborationsValidator,

      },
    },
    {
      plugin: _exports,
      options: {
        service: ProducerService,
        playlistService,
        validator: ExportValidator,
      },
    },
    {
      plugin: uploads,
      options: {
        service: storageService,
        validator: ImageValidator,
      },
    },
    {
      plugin: likes,
      options: {
        service: likeService,
      },
    },
  ]);

  await server.start();
  console.log(`Server berjalan pada ${server.info.uri}`);
};

init();
