const { nanoid } = require('nanoid');
const { Pool } = require('pg');
const bcrypt = require('bcrypt');

const InvariantError = require('../../../exceptions/InvariantError');
const AuthenticationError = require('../../../exceptions/AuthenticationError');

class UsersServices {
  constructor() {
    this._pool = new Pool();
  }

  async addUser(username, password, fullname) {
    await this.verifyUser(username);

    const id = `user-${nanoid(16)}`;
    const createdAt = new Date().toISOString();
    const updatedAt = createdAt;
    const hashedPassword = await bcrypt.hash(password, 10);

    const query = {
      text: 'INSERT INTO users VALUES($1, $2, $3, $4, $5, $6) RETURNING id',
      values: [id, username, hashedPassword, fullname, createdAt, updatedAt],
    };

    const result = await this._pool.query(query);

    if (!result.rowCount) {
      throw new InvariantError('Failed to create user');
    }
    return result.rows[0].id;
  }

  async verifyUser(username) {
    const query = {
      text: 'SELECT * FROM users WHERE username = $1',
      values: [username],
    };

    const result = await this._pool.query(query);

    if (result.rowCount > 0) {
      throw new InvariantError(`${username} already exists`);
    }
  }

  async verifyUserCredentials(username, password) {
    const query = {
      text: 'SELECT * FROM users WHERE username = $1',
      values: [username],
    };

    const result = await this._pool.query(query);

    if (result.rowCount === 0) {
      throw new AuthenticationError(`${username} does not exist`);
    }

    const user = result.rows[0];
    const isValid = await bcrypt.compare(password, user.password);

    if (!isValid) {
      throw new AuthenticationError('Invalid credentials');
    }
    return user;
  }
}

module.exports = UsersServices;
