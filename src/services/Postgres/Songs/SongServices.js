/* eslint-disable camelcase */

const { Pool } = require('pg');
const { nanoid } = require('nanoid');

const { mapSongById } = require('../../../Utility/Song');
const InvariantError = require('../../../exceptions/InvariantError');
const NotFoundError = require('../../../exceptions/NotFoundError');

class SongServices {
  constructor() {
    this._pool = new Pool();
  }

  async postSong({
    title, year, performer, genre, duration, albumId,
  }) {
    const id = `song-${nanoid(16)}`;
    const createdAt = new Date().toISOString();
    const updatedAt = createdAt;
    const query = {
      text: 'INSERT INTO songs VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING id',
      values: [id, title, year, genre, performer, duration, albumId, createdAt, updatedAt],
    };

    const result = await this._pool.query(query);
    if (!result.rows[0].id) {
      throw new InvariantError('Song was not added');
    }
    return result.rows[0].id;
  }

  async getSongs(title, performer) {
    let queryCondition = '';

    switch (true) {
      case (title != null && performer != null):
        queryCondition = ` WHERE lower(title) like '%${title}%' and lower(performer) like '%${performer}%'`;
        break;
      case (title != null && performer == null):
        queryCondition = ` WHERE lower(title) like '%${title}%'`;
        break;
      case (title == null && performer != null):
        queryCondition = ` WHERE lower(performer) like '%${performer}%'`;
        break;
      default:
        break;
    }

    const result = await this._pool.query(`SELECT id, title, performer FROM songs ${queryCondition}`);
    return result.rows;
  }

  async getSongById(id) {
    const query = {
      text: 'SELECT * FROM songs WHERE id = $1',
      values: [id],
    };
    const result = await this._pool.query(query);
    if (!result.rows.length) {
      throw new NotFoundError('Song was not found');
    }
    return result.rows.map(mapSongById)[0];
  }

  async putSong(id, {
    title, year, performer, genre, duration,
  }) {
    const query = {
      text: 'UPDATE songs SET title = $1, year = $2, genre = $3, performer = $4, duration = $5 WHERE id = $6 RETURNING id',
      values: [title, year, genre, performer, duration, id],

    };
    const result = await this._pool.query(query);

    if (!result.rows.length) {
      throw new NotFoundError('Song was not updated');
    }
  }

  async deleteSong(id) {
    const query = {
      text: 'DELETE FROM songs WHERE id = $1 RETURNING id',
      values: [id],
    };
    const result = await this._pool.query(query);
    if (!result.rows.length) {
      throw new NotFoundError('Song was not deleted');
    }
  }
}

module.exports = SongServices;
