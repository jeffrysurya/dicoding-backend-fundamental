const joi = require('joi');

const postAuthentication = joi.object({
  username: joi.string().max(20).required(),
  password: joi.string().required(),
});

const putAuthentication = joi.object({
  refreshToken: joi.string().required(),
});

const deleteAuthentication = joi.object({
  refreshToken: joi.string().required(),
});

module.exports = { putAuthentication, postAuthentication, deleteAuthentication };
