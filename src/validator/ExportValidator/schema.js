const joi = require('joi');

const ExportPlaylistPayloadSchema = joi.object({
  targetEmail: joi.string().email({ tlds: true }).required(),
});

module.exports = ExportPlaylistPayloadSchema;
