const mapSongById = ({
  id,
  title,
  year,
  performer,
  genre,
  duration,
  album_id,
}) => ({
  id,
  title,
  year: Number(year),
  performer,
  genre,
  duration: Number(duration),
  album_id,
});

const mapSongGetAll = ({
  id,
  title,
  performer,
}) => ({
  id,
  title,
  performer,
});
module.exports = { mapSongGetAll, mapSongById };
