const mapAlbumGetAll = ({
  id,
  name,
  year,
}) => ({
  id,
  name,
  year: Number(year),
});

module.exports = { mapAlbumGetAll };
