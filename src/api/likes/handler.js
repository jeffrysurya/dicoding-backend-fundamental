const ClientError = require('../../exceptions/ClientError');

class LikesHandler {
  constructor(service, validator) {
    this._service = service;
    this._validator = validator;

    this.postAlbumLike = this.postAlbumLike.bind(this);
    this.getAlbumLikesById = this.getAlbumLikesById.bind(this);
  }

  async postAlbumLike(request, h) {
    try {
      let result;

      const { albumId } = request.params;
      const { id: credentialId } = request.auth.credentials;
      const idCredential = credentialId.id;

      await this._service.checkAlbumId(albumId);

      const liked = await this._service.checkAlbumLike(idCredential, albumId);

      if (!liked) {
        await this._service.addAlbumLike(idCredential, albumId);
        result = `Like ${albumId}`;
      } else {
        await this._service.deleteAlbumLike(idCredential, albumId);
        result = `Unlike ${albumId}`;
      }

      const response = h.response({
        status: 'success',
        message: result,
      });

      response.code(201);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        }); response.code(error.statusCode);
        return response;
      }
      const response = h.response({
        status: 'error',
        message: 'Sorry, something went wrong',
      }); response.code(500);
      return response;
    }
  }

  async getAlbumLikesById(request, h) {
    const { albumId } = request.params;
    const result = await this._service.getAlbumLikesById(albumId);

    const response = h.response({
      status: 'success',
      data: {
        likes: result.likes,
      },
    });

    response.code(200);

    if (result.cache) {
      response.header('X-Data-Source', 'cache');
    }

    return response;
  }
}

module.exports = LikesHandler;
