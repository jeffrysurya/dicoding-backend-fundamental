const ClientError = require('../../exceptions/ClientError');

class UploadHandler {
  constructor(service, validator) {
    this._service = service;
    this._validator = validator;

    this.postUploadImage = this.postUploadImage.bind(this);
  }

  async postUploadImage(request, h) {
    try {
      const data = request.payload;
      const { albumId } = request.params;

      this._validator.validateImageHeaders(data.cover.hapi.headers);

      const filename = await this._service.writeFile(data.cover, data.cover.hapi);
      await this._service.updateCoverUrl(albumId, `http://${process.env.HOST}:${process.env.PORT}/albums/${albumId}/covers/${filename}`);

      const response = h.response({
        status: 'success',
        message: 'Album Cover Succesully Uploaded',
        data: {
          coverUrl: `http://${process.env.HOST}:${process.env.PORT}/albums/${albumId}/covers/${filename}`,
        },
      });
      response.code(201);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        }); response.code(error.statusCode);
        return response;
      }
      const response = h.response({
        status: 'error',
        message: 'Sorry, something went wrong',
      }); response.code(500);
      console.log(error);
      return response;
    }
  }
}

module.exports = UploadHandler;
