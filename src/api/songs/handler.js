const ClientError = require('../../exceptions/ClientError');

class SongHandler {
  constructor(service, validator) {
    this._service = service;
    this._validator = validator;

    this.PostSongHandler = this.PostSongHandler.bind(this);
    this.GetSongHandler = this.GetSongHandler.bind(this);
    this.GetSongByIdHandler = this.GetSongByIdHandler.bind(this);
    this.PutSongByIdHandler = this.PutSongByIdHandler.bind(this);
    this.DeleteSongByIdHandler = this.DeleteSongByIdHandler.bind(this);
  }

  async PostSongHandler(request, h) {
    try {
      this._validator.validateSongPayload(request.payload);

      const {
        title, year, genre, performer, duration, albumId,
      } = request.payload;

      const songId = await this._service.postSong({
        title, year, genre, performer, duration, albumId,
      });

      const response = h.response({
        status: 'success',
        message: `Song with id ${songId} was added`,
        data: {
          songId,
        },
      });
      response.code(201);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        }); response.code(error.statusCode);
        return response;
      }
      const response = h.response({
        status: 'error',
        message: 'Sorry, something went wrong',
      }); response.code(500);
      return response;
    }
  }

  async GetSongHandler(request, h) {
    try {
      const { title, performer } = request.query;

      const songs = await this._service.getSongs(title, performer);
      const response = h.response({
        status: 'success',
        message: 'sucessfully fetched songs',
        data: {
          songs,
        },
      }); response.code(200);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        }); response.code(error.statusCode);
        return response;
      }
      const response = h.response({
        status: 'error',
        message: 'Sorry, something went wrong',
      }); response.code(500);
      return response;
    }
  }

  async GetSongByIdHandler(request, h) {
    try {
      const { id } = request.params;
      const song = await this._service.getSongById(id);

      const response = h.response({
        status: 'success',
        data: {
          song,
        },
      }); response.code(200);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        }); response.code(error.statusCode);
        return response;
      }
      const response = h.response({
        status: 'error',
        message: 'Sorry, something went wrong',
      }); response.code(500);
      return response;
    }
  }

  async PutSongByIdHandler(request, h) {
    try {
      this._validator.validateSongPayload(request.payload);
      const { id } = request.params;

      await this._service.putSong(id, request.payload);

      const response = h.response({
        status: 'success',
        message: 'Song updated',
      }); response.code(200);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        }); response.code(error.statusCode);
        return response;
      }
      const response = h.response({
        status: 'error',
        message: 'Sorry, something went wrong',
      }); response.code(500);
      return response;
    }
  }

  async DeleteSongByIdHandler(request, h) {
    try {
      const { id } = request.params;
      await this._service.deleteSong(id);

      const response = h.response({
        status: 'success',
        message: 'Song deleted',
      }); response.code(200);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        }); response.code(error.statusCode);
        return response;
      }
      const response = h.response({
        status: 'error',
        message: 'Sorry, something went wrong',
      }); response.code(404);
      return response;
    }
  }
}
module.exports = SongHandler;
