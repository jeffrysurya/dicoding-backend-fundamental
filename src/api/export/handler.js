const ClientError = require('../../exceptions/ClientError');

class ExportsHandler {
  constructor(service, playlistService, validator) {
    this._service = service;
    this._playlistService = playlistService;
    this._validator = validator;

    this.postExportPlaylist = this.postExportPlaylist.bind(this);
  }

  async postExportPlaylist(request, h) {
    try {
      const { playlistId } = request.params;
      const { id: credentialId } = request.auth.credentials;
      this._validator.validateExportPlaylistPayload(request.payload);

      await this._playlistService.verifyPlaylistOwner(playlistId, credentialId.id);

      const message = {
        playlistId,
        targetEmail: request.payload.targetEmail,
      };

      await this._service.sendMessage('export:playlists', JSON.stringify(message));

      const response = h.response({
        status: 'success',
        message: `Export ${playlistId} in queue`,
      }); response.code(201);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        }); response.code(error.statusCode);
        return response;
      }
      const response = h.response({
        status: 'error',
        message: 'Sorry, something went wrong',
      }); response.code(500);
      return response;
    }
  }
}

module.exports = ExportsHandler;
