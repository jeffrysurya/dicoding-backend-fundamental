const ClientError = require('../../exceptions/ClientError');

class AlbumHandler {
  constructor(service, validator) {
    this._service = service;
    this._validator = validator;

    this.postAlbumHandler = this.postAlbumHandler.bind(this);
    this.getAlbumsHandler = this.getAlbumsHandler.bind(this);
    this.putAlbumByIdHandler = this.putAlbumByIdHandler.bind(this);
    this.deleteAlbumByIdHandler = this.deleteAlbumByIdHandler.bind(this);
  }

  async postAlbumHandler(request, h) {
    try {
      this._validator.validateAlbumPayload(request.payload);
      const { name = 'uname', year } = request.payload;

      const albumId = await this._service.postAlbum({ name, year });

      const response = h.response({
        status: 'success',
        message: 'Album added',
        data: {
          albumId,
        },
      }); response.code(201);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        }); response.code(error.statusCode);
        return response;
      }
      const response = h.response({
        status: 'error',
        message: 'Sorry, something went wrong',

      }); response.code(500);
      return response;
    }
  }

  async getAlbumsHandler(request, h) {
    try {
      const { id } = request.params;
      const album = await this._service.getAlbumsById(id);
      const response = h.response({
        status: 'success',
        message: `${id} was Found`,
        data: {
          album,
        },
      }); response.code(200);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        }); response.code(error.statusCode);
        return response;
      }
      const response = h.response({
        status: 'error',
        message: 'Sorry, something went wrong',

      }); response.code(500);
      return response;
    }
  }

  async putAlbumByIdHandler(request, h) {
    try {
      this._validator.validateAlbumPayload(request.payload);

      const { name, year } = request.payload;

      const { id } = request.params;

      await this._service.editAlbumById(id, { name, year });

      const response = h.response({
        status: 'success',
        message: `${id} was Edited`,
      }); response.code(200);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        }); response.code(error.statusCode);
        return response;
      }
      const response = h.response({
        status: 'error',
        message: 'Sorry, something went wrong',

      }); response.code(500);
      return response;
    }
  }

  async deleteAlbumByIdHandler(request, h) {
    try {
      const { id } = request.params;

      await this._service.deleteAlbumById(id);

      const response = h.response({
        status: 'success',
        message: `${id} was Deleted`,
      }); response.code(200);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        }); response.code(error.statusCode);
        return response;
      }
      const response = h.response({
        status: 'error',
        message: 'Sorry, something went wrong',

      }); response.code(500);
      return response;
    }
  }
}
module.exports = AlbumHandler;
