const ClientError = require('../../exceptions/ClientError');

class UserHandler {
  constructor(service, validator) {
    this._service = service;
    this._validator = validator;

    this.postUser = this.postUser.bind(this);
  }

  async postUser(request, h) {
    try {
      this._validator.validateUserPayload(request.payload);

      const { username, password, fullname } = request.payload;
      const userId = await this._service.addUser(username, password, fullname);

      const response = h.response({
        status: 'success',
        message: `User ${username} created successfully`,
        data: {
          userId,
        },
      }); response.code(201);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        }); response.code(error.statusCode);
        return response;
      }
      const response = h.response({
        status: 'error',
        message: 'Sorry, something went wrong',
      }); response.code(500);
      return response;
    }
  }
}
module.exports = UserHandler;
