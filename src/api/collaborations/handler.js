const ClientError = require('../../exceptions/ClientError');

class CollaborationHandler {
  constructor(collaborationService, playlistService, validator) {
    this._collaborationService = collaborationService;
    this._playlistService = playlistService;
    this._validator = validator;

    this.postCollaboration = this.postCollaboration.bind(this);
    this.deleteCollaboration = this.deleteCollaboration.bind(this);
  }

  async postCollaboration(request, h) {
    try {
      this._validator.validateCollaborationPayload(request.payload);

      const { id: credentialId } = request.auth.credentials;
      const { playlistId, userId } = request.payload;

      await this._playlistService.checkUserAvailability(userId);
      await this._playlistService.verifyPlaylistOwner(playlistId, credentialId.id);

      const collaborationId = await this._collaborationService.addCollaboration(playlistId, userId);

      const response = h.response({
        status: 'success',
        message: `${collaborationId} with user ${userId} added successfully`,
        data: {
          collaborationId,
        },
      }); response.code(201);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        }); response.code(error.statusCode);
        return response;
      }
      const response = h.response({
        status: 'error',
        message: 'Sorry, something went wrong',
      }); response.code(500);
      return response;
    }
  }

  async deleteCollaboration(request, h) {
    try {
      this._validator.validateCollaborationPayload(request.payload);

      const { id: credentialId } = request.auth.credentials;
      const { playlistId, userId } = request.payload;

      await this._playlistService.verifyPlaylistOwner(playlistId, credentialId.id);
      await this._collaborationService.deleteCollaboration(playlistId, userId);

      const response = h.response({
        status: 'success',
        message: `Collaboration with id ${playlistId} deleted successfully`,
      }); response.code(200);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        }); response.code(error.statusCode);
        return response;
      }
      const response = h.response({
        status: 'error',
        message: 'Sorry, something went wrong',
      }); response.code(500);
      return response;
    }
  }
}
module.exports = CollaborationHandler;
