const ClientError = require('../../exceptions/ClientError');

class Playlists {
  constructor(service, validator) {
    this._service = service;
    this._validator = validator;

    this.postPlaylist = this.postPlaylist.bind(this);
    this.getPlaylistHandler = this.getPlaylistHandler.bind(this);
    this.deletePlaylist = this.deletePlaylist.bind(this);
    this.postSongPlayList = this.postSongPlayList.bind(this);
    this.getSongPlaylist = this.getSongPlaylist.bind(this);
    this.deleteSongPlaylist = this.deleteSongPlaylist.bind(this);
    this.getActivity = this.getActivity.bind(this);
  }

  async postPlaylist(request, h) {
    try {
      this._validator.validatePostPlaylistPayload(request.payload);

      const { name } = request.payload;
      const { id: credentialId } = request.auth.credentials;

      const playlistId = await this._service.addPlaylist({
        name, owner: credentialId.id,
      });

      const response = h.response({
        status: 'success',
        message: `Playlist ${playlistId} created successfully`,
        data: {
          playlistId,
        },
      }); response.code(201);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        }); response.code(error.statusCode);
        return response;
      }
      const response = h.response({
        status: 'error',
        message: 'Sorry, something went wrong',
      }); response.code(500);

      return response;
    }
  }

  async getPlaylistHandler(request, h) {
    try {
      const { id: credentialId } = request.auth.credentials;
      const playlists = await this._service.getPlaylist(credentialId.id);

      const response = h.response({
        status: 'success',
        data: {
          playlists,
        },
      }); response.code(200);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        }); response.code(error.statusCode);
        return response;
      }
      const response = h.response({
        status: 'error',
        message: 'Sorry, something went wrong',
      }); response.code(500);
      return response;
    }
  }

  async deletePlaylist(request, h) {
    try {
      const { playlistId } = request.params;
      const { id: credentialId } = request.auth.credentials;

      await this._service.verifyPlaylistOwner(playlistId, credentialId.id);
      await this._service.deletePlaylist(playlistId);

      const response = h.response({
        status: 'success',
        message: `Playlist ${playlistId} deleted successfully`,
      }); response.code(200);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        }); response.code(error.statusCode);
        return response;
      }
      const response = h.response({
        status: 'error',
        message: 'Sorry, something went wrong',
      }); response.code(500);
      return response;
    }
  }

  async postSongPlayList(request, h) {
    try {
      this._validator.validatePostSongPlaylistPayload(request.payload);

      const { playlistId } = request.params;
      const { songId } = request.payload;
      const { id: credentialId } = request.auth.credentials;

      await this._service.verifySongId(songId);
      await this._service.verifyPlaylistAuth(playlistId, credentialId.id);
      await this._service.addSongPlaylist(playlistId, songId);

      const response = h.response({
        status: 'success',
        message: `Song ${songId} added to playlist ${playlistId} successfully`,
      }); response.code(201);

      await this._service.addActivity(playlistId, songId, credentialId.id, 'add');

      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        }); response.code(error.statusCode);
        return response;
      }
      const response = h.response({
        status: 'error',
        message: 'Sorry, something went wrong',
      }); response.code(500);
      return response;
    }
  }

  async getSongPlaylist(request, h) {
    try {
      const { playlistId } = request.params;
      const { id: credentialId } = request.auth.credentials;
      await this._service.verifyPlaylistAuth(playlistId, credentialId.id);
      const playlist = await this._service.getSongPlaylist({ playlistId });

      const response = h.response({
        status: 'success',
        data: {
          playlist,
        },
      }); response.code(200);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        }); response.code(error.statusCode);
        return response;
      }
      const response = h.response({
        status: 'error',
        message: 'Sorry, something went wrong',
      }); response.code(500);
      return response;
    }
  }

  async deleteSongPlaylist(request, h) {
    try {
      this._validator.validateDeleteSongPlaylistPayload(request.payload);

      const { playlistId } = request.params;
      const { songId } = request.payload;
      const { id: credentialId } = request.auth.credentials;

      await this._service.verifyPlaylistAuth(playlistId, credentialId.id);
      await this._service.deleteSongPlaylist(playlistId, songId);

      await this._service.addActivity(playlistId, songId, credentialId.id, 'delete');

      const response = h.response({
        status: 'success',
        message: `Song ${songId} deleted from playlist ${playlistId} successfully`,
      }); response.code(200);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        }); response.code(error.statusCode);
        return response;
      }
      const response = h.response({
        status: 'error',
        message: 'Sorry, something went wrong',
      }); response.code(500);
      return response;
    }
  }

  async getActivity(request, h) {
    try {
      const { playlistId } = request.params;
      const { id: credentialId } = request.auth.credentials;

      await this._service.verifyPlaylistAuth(playlistId, credentialId.id);

      const activities = await this._service.getActivities(playlistId);

      const response = h.response({
        status: 'success',
        data: activities,
      }); response.code(200);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        }); response.code(error.statusCode);
        return response;
      }
      const response = h.response({
        status: 'error',
        message: 'Sorry, something went wrong',
      }); response.code(500);
      return response;
    }
  }
}

module.exports = Playlists;
